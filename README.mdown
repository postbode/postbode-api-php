# Postbode.nu PHP API Client #


## PostTypes ##


## Enveloppen ##
De functie Postbode::setEnvelopeID() geeft de mogelijkheid de te gebruiken envelop te selecteren, er zijn 2 standaard enveloppen beschikbaar;

+ Envelop 1: Dit is een envelop voor de Standaad verzending, met een Postbode.nu logo op de voorzijde.
+ Envelop 2: Dit is een envelop voor de Premium / Blanco verzending

Als er een eigen ontwerp van enveloppen in huisstijl is aangeleverd, krijg je na goedkeuring een eigen uniek envelopnummer. Dit nummer kan vervolgens in de API weer gebruikt worden om de brief in de juiste envelop te stoppen.


```php
	$oPostbode->setEnvelopeID(1);
```

## Vragen of Opmerkingen? ##
Stuur ons een bericht op postkantoor@postbode.nu of bel ons op 078 - 780 23 59